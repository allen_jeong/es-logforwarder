#!/usr/bin/env python
from elasticsearch import Elasticsearch
from dateutil.parser import parse
from ConfigParser import SafeConfigParser
from random import shuffle
from datetime import date, datetime, timedelta
from pytz import timezone
import time
import re
import logging
import time
import threading
import os
import json
import socket
import sys
import random
import hashlib
import glob
#
logging.basicConfig(level=logging.WARN, format='%(asctime)s - %(levelname)s - %(message)s')


class esForwarder:
    def __init__(self, conf='es_forwarder.ini'):
        self.dir_path = os.path.dirname(os.path.realpath(__file__)) + '/'
        self.tz = {}
        self.progs = {}
        self.ehosts = []
        self.sections = {}
        self.threads = []
        self.sthreads = []
        self.hostname = socket.gethostname()
        self.log_count = 0
        self.log_format = {}
        self.geoip = {}
        self.sampling_rate = {}
        self.thread_size = 20
        # if lines in the buffer more than buffer_size, then send to ES
        self.buffer_size = 20
        # Even lines in the buffer less than buffer_size, if it waited more than checkpoint second, then send to ES
        self.checkpoint = 1
        self.doc_type = {}

        ' load config from ini'
        parser = SafeConfigParser()
        '''load bundle patterns'''
        self.bundle_patterns = {}
        parser.read(self.dir_path + 'patterns.ini')
        for name, pattern in parser.items('log_patterns'):
            self.bundle_patterns[name] = pattern

        parser = SafeConfigParser()
        parser.read(self.dir_path + conf)
        for section_name in parser.sections():
            if section_name == 'global':
                ePort = parser.get('global', 'elasticsearch_port')
                tehs = parser.get('global', 'elasticsearch_host').split(',')
                for host in tehs:
                    ehost = {"host": host.lstrip(), "port": ePort}
                    self.ehosts.append(ehost)
            else:
                '''read all patterns from each section'''
                parsed_patn = parser.get(section_name, 'pattern')
                if parsed_patn.startswith('bundle_'):
                    pattern = self.bundle_patterns[parsed_patn.split('_')[1]]
                else:
                    pattern = parsed_patn
                self.sections[section_name] = (
                    parser.get(section_name, 'file_path'), parser.get(section_name, 'ts_field'),
                    parser.get(section_name, 'index_name'), parser.get(section_name, 'ts_type'))
                try:
                    self.sampling_rate[section_name] = int(parser.get(section_name, 'sampling_rate')) / 100.0
                    logging.info(self.sampling_rate)
                except Exception as e:
                    # default sampling rate is 100%
                    logging.info('sampling rate exception. {}'.format(e.message))
                    self.sampling_rate[section_name] = [1, 1]

                self.progs[section_name] = re.compile(pattern)

                # finding log_format
                try:
                    self.log_format[section_name] = parser.get(section_name, 'log_format')
                except Exception as e:
                    self.log_format[section_name] = 'text'
                                   # finding document type
                try:
                    self.doc_type[section_name] = parser.get(section_name, 'doc_type')
                except Exception as e:
                    self.doc_type[section_name] = 'log'

                # finding custom timezone
                try:
                    self.tz[section_name] = parser.get(section_name, 'ts_tz')
                except Exception as e:
                    self.tz[section_name] = False

                # find geoip
                if 'geoip' in parser.options(section_name):
                    logging.info('found geoip in section {}'.format(section_name))
                    self.geoip[section_name] = parser.get(section_name, 'geoip')
                    logging.info('geoip[{}] = {}'.format(section_name, self.geoip[section_name]))
                    try:
                        self.netacuity = parser.get(section_name, 'netacuity')
                    except Exception as e:
                        logging.warn('geoip needs netacuity. {}'.format(e.message))
                else:
                    self.geoip[section_name] = False

        self.suff_eshost()
        self.esClient = Elasticsearch(hosts=self.ehosts, maxsize=20)

    # find greatest common divisor
    # def gcd(self, val):
    #     if val > 100:
    #         logging.warn('Sampling rate should be less than 100')
    #         sys.exit(2)
    #     a = 100
    #     b = val
    #     if a < b:
    #         (a, b) = (b, a)
    #     while b != 0:
    #         (a, b) = (b, a % b)
    #     logging.info("sampling rate is {} out of {}".format(val/a ,100/a))
    #     # deviding each value by a GCD. so x out of y can be returned
    #     return [val/a, 100/a]

    def check_checkpoint(self, linesinbuffer, ttl):
        # if lines in buff less than buffer
        if linesinbuffer < self.buffer_size:
            # if checkpoint (ttl) is passed, then send it
            if ttl < int(time.time()):
                if linesinbuffer > 0:
                    logging.info('current({}) time is greater than ttl({}), send {} lines'.format(int(time.time()), ttl,
                                                                                              linesinbuffer))
                    send_log = False
                else:
                    send_log = True
            else:
                logging.debug('less than ttl, wait for next log. linesinbuff: {}'.format(linesinbuffer))
                send_log = True
        else:
            logging.debug('{} lines in the buff'.format(linesinbuffer))
            send_log = False

        return send_log

    def get_geoip(self, ip):
        return False

    def suff_eshost(self):
        """
        shuffle ES host before it use or target ES host is not working
        :return:
        """
        shuffle(self.ehosts)

    # enforce casting for digit
    def is_digit(self, val):
        val = val.strip()
        logging.debug('is digit {}'.format(val))
        match = re.search('[^0-9.-]', val)
        if not match:
            match = re.search('[.]', val)
            try:
                if match:
                    return float(val)
                else:
                    return int(val)
            except Exception as e:
                return val
        else:
            return val

    def convert_tz(self,cur_dt, key):
        custom_tz_in_second = timezone(self.tz[key]).utcoffset(cur_dt).total_seconds()
        cur_utc = cur_dt - timedelta(seconds=custom_tz_in_second)
        return cur_utc

    def find_newest_file(self, log_patn):
        cur_mtime = 0
        cur_file = ''
        for tfile in glob.glob(log_patn):
            cf = os.stat(tfile).st_mtime
            if cf > cur_mtime:
                cur_mtime = cf
                cur_file = tfile
        logging.info('found file {}'.format(cur_file))
        return cur_file

    def get_md5(self, tohash):
        m = None
        m = hashlib.md5()
        m.update(tohash)
        return m.hexdigest()

    def save_offset(self, cur_offset_file, offset):
        # update offset
        tf = open(cur_offset_file, 'w')
        logging.debug('f tell {}'.format(offset))
        tf.write(str(offset))
        tf.close()

    def read_log(self, key):
        saved_args = locals()
        log_path, timestamp, index_name, ts_type = self.sections[key]
        logging.warn('start reading file from {}'.format(log_path))
        seek_new = False

        line_buff = []
        ttl = int(time.time()) + 1
        old_process_log = None
        burst = False
        while True:  # handle moved/truncated files by allowing to reopen
            # find newest file in the pattern
            process_log = self.find_newest_file(log_path)
            if not old_process_log:
                old_process_log = process_log
            if old_process_log != process_log:
                old_process_log = process_log
                seek_new = True
            else:
                if burst:
                    seek_new = True
                    burst = False
                else:
                    seek_new = False

            # to avoid using wild card as file name, use md5 hash
            file_md5 = self.get_md5(log_path)
            cur_offset_file = '/tmp/.' + file_md5 + '.offset'
            logging.warn('currently using log : {}'.format(process_log))
            # check file
            if not os.path.isfile(process_log):
                logging.warn('{} does not exists. End up thread')
                return True
            with open(process_log) as f:
                if not seek_new:  # reopened files must not seek end
                    try:
                        of = open(cur_offset_file)
                        cur_offset = int(of.readline())
                        logging.info('found current offset {}'.format(cur_offset))
                        of.close()
                    except Exception as e:
                        logging.info('no file exists. offset reset to 0. {}'.format(e.message))
                        cur_offset = 0

                else:
                    cur_offset = 0
                # seek file
                f.seek(cur_offset)
                check_seek = 0
                while True:  # line reading loop
                    line = f.readline()
                    if not line:
                        try:
                            if f.tell() > os.path.getsize(process_log):
                                # if file has recreated. reopen it
                                # rotation occurred (copytruncate/create)
                                f.close()
                                burst = True
                                break
                            else:
                                check_seek += 1
                                # if no log for last 60 seconds, then find newest file again
                                if check_seek > 60:
                                    self.save_offset(cur_offset_file, f.tell())
                                    break
                        except Exception as e:
                            logging.warn('Exception during file reading. {}'.format(e.message))
                            burst = False
                            self.save_offset(cur_offset_file, f.tell())
                            # rotation occurred but new file still not created
                            break  # wait 1 second and retry
                        time.sleep(1)

                    # To pass empty line
                    if len(line) > 0:
                        # if log delivery has failed, looping till it has done
                        if random.random() <= self.sampling_rate[key]:
                            try:
                                if self.log_format[key] == 'text':
                                    logging.debug('parsing text. {}'.format(line))
                                    index_name, doc = self.parse_log(line, key)
                                elif self.log_format[key] == 'json':
                                    logging.debug('parsing json. {}'.format(line))
                                    index_name, doc = self.parse_json(line, key)
                                else:
                                    logging.warn('log_format {} is not supported on section {}'.format(self.log_format[key],key))
                                    sys.exit(1)

                                doc['type'] = self.doc_type[key]
                                # add file offset
                                doc['offset'] = f.tell()
                                # add cur log file name
                                doc['file'] = process_log

                                logging.debug('index: {}, body: {}'.format(index_name, json.dumps(doc)))
                                bidx = {'index': {'_index': index_name, '_type': self.doc_type[key]}}
                                line_buff.append(json.dumps(bidx))
                                line_buff.append(json.dumps(doc))
                                self.log_count += 1
                            except Exception as e:
                                logging.warn('line append has failed. {}'.format(e.message))
                        else:
                            logging.debug('this line {} has passed'.format(line))
                    else:
                        logging.debug('pass this line. line: {}'.format(line))

                    send_log = self.check_checkpoint(len(line_buff) / 2, ttl)
                    while not send_log:
                        logging.info('{} lines are sending to ES'.format(len(line_buff) / 2))
                        send_log = self.send_log(index_name, line_buff, key)
                        if send_log:
                            self.save_offset(cur_offset_file, f.tell())
                            # delete buff
                            logging.debug('delete line_buff')
                            line_buff = []
                            #del line_buff[:]
                            if self.log_count > 1000:
                                logging.warn('log processed {} times for {}'.format(self.log_count,key))
                                self.log_count = 0
                            # reset ttl. current time + checkpoint seconds. on the other hand the checkpoint is 1 second
                            ttl = int(time.time()) + self.checkpoint
                            break
                        else:
                            logging.warn('Write to ES has failed, retrying..')

    def parse_json(self, line, key):
        logging.debug('parse json log. line:{} '.format(line))
        log_path, timestamp, index_name, ts_type = self.sections[key]
        mdict = json.loads(line)
        try:
            if ts_type == 'unix':
                t_timestamp = datetime.fromtimestamp(float(mdict[timestamp]))
            elif ts_type == 'nginx':
                t_timestamp = parse(mdict[timestamp][:11] + ' ' + mdict[timestamp][12:])
            else:
                t_timestamp = parse(mdict[timestamp])

            # update timezone if needed
            if self.tz[key]:
                mdict['@timestamp'] = self.convert_tz(t_timestamp, key).isoformat()
            else:
                mdict['@timestamp'] = t_timestamp.isoformat()

        except Exception as e:
            logging.debug('parsing timestamp field {} has failed. {}'.format(mdict[timestamp], e.message))

        logging.debug('add host name')
        mdict['hostname'] = self.hostname
        ''' pop '''
        # using domain name as a PoP
        logging.debug('add pop name')
        try:
            mdict['pop'] = self.hostname.split('.')[-2]
        except Exception as e:
            logging.debug('failed assign pop')
            mdict['pop'] = ''

        mdict['logfile'] = log_path
        # change index every day
        index_name = index_name + '-' + date.today().isoformat().replace('-', '.')
        logging.debug('return indexname: {}, dictionary: {}'.format(index_name, json.dumps(mdict)))
        return [index_name, mdict]

    def parse_log(self, line, key):
        logging.debug('send log')
        log_path, timestamp, index_name, ts_type = self.sections[key]
        prog = self.progs[key]
        m = prog.match(line)
        try:
            mdict = m.groupdict()
        except Exception as e:
            logging.warn('Exception for line {} for log {} '.format(line,log_path))
            logging.debug('message: {}, error: {}'.format(e.message, e.args))
            return True

        # apache, nginx datetime can't be parsed.
        try:
            if ts_type == 'unix':
                t_timestamp = datetime.fromtimestamp(float(mdict[timestamp]))
            elif ts_type == 'nginx':
                t_timestamp = parse(mdict[timestamp][:11] + ' ' + mdict[timestamp][12:])
            else:
                t_timestamp = parse(mdict[timestamp])

            # update timezone if needed

            if self.tz[key]:
                mdict['@timestamp'] = self.convert_tz(t_timestamp, key).isoformat()
            else:
                mdict['@timestamp'] = t_timestamp.isoformat()

        except Exception as e:
            logging.debug('parsing timestamp field {} has failed. {}'.format(mdict[timestamp], e.message))


        # While it has @timestamp, doesn't need another timestamp field. to have compatibility w/ another log. remove
        # timestamp field
        mdict.pop(timestamp, None)

        # additional fields for admin
        mdict['hostname'] = self.hostname
        ''' pop '''

        # using domain name as a PoP
        try:
            mdict['pop'] = self.hostname.split('.')[-2]
        except Exception as e:
            logging.debug('failed to parse PoP from a hostname: {}. {}'.format(self.hostname, e.message))
        # keep response time in number. special needs for local situation
        try:
            if mdict['upstreamtime'] == '-':
                mdict['upstreamtime'] = '0'
        except Exception as e:
            logging.debug('upstreamtime does not exist on key {}. {}'.format(key,e.message))

        ''' casting number '''
        for mkey in mdict.keys():
            mdict[mkey] = self.is_digit(mdict[mkey])
        try:
            if self.geoip[key]:
                isGeoIP = self.get_geoip(mdict[self.geoip[key]])
                if isGeoIP:
                    mdict['geoip'] = isGeoIP
        except Exception as e:
            logging.debug('Do not use geoip')

        # add log file path
        mdict['file'] = log_path
        # change index every day
        index_name = index_name + '-' + date.today().isoformat().replace('-', '.')
        return [index_name, mdict]

    def send_log(self, index_name, body, key):
        try:
            sthread = True
            while sthread:
                if len(self.sthreads) < self.thread_size:
                    logging.debug('start thread for index : {}, body: {}, key: {}'.format(index_name, json.dumps(body), key))
                    tl = threading.Thread(target=self.thr_send_log, args=(index_name, body, key,))
                    self.sthreads.append(tl)
                    tl.start()
                    logging.info('new thread started current size {} for {}'.format(len(self.sthreads), key))
                    sthread = False
                else:
                    logging.info('too many threads {}. Waiting for finishing'.format(len(self.sthreads)))
                    time.sleep(1)
                    sthread = True
            return True
        except Exception as e:
            # suffle elasticsearch host
            self.suff_eshost()
            logging.warn('Write on ElasticSearch has failed. Retry in a second. {}'.format(e.message))
            time.sleep(1)
            return False

    def thr_send_log(self, index_name, tbody, key):
        logging.debug('thr_send_log for {}, index_name: {}, body: {}'.format(key, index_name, json.dumps(tbody)))
        thr_send_log = False
        while not thr_send_log:
            try:
                logging.debug('final sending body for {}: body: {}'.format(key, json.dumps(tbody)))
                response = self.esClient.bulk(index=index_name, doc_type=self.doc_type[key], body='\n'.join(tbody))
                logging.debug('bulk insert return : {}'.format(response))
                logging.debug('list of sthreads. {}'.format(str(self.sthreads)))
                self.sthreads.pop()
                thr_send_log = True
            except Exception as e:
                logging.debug('Exception on thr_send_log, {}'.format(e.message))
                time.sleep(1)
                thr_send_log = False

    def run(self):
        for key in self.sections.keys():  # start log delivery for all in es_forwarder.ini
            logging.warn("Starting Thread for {}".format(key))
            try:
                t = threading.Thread(target=self.read_log, args=(key,))
                self.threads.append(t)
                t.start()
                logging.warn('Started thread for {}'.format(key))
            except Exception as e:
                logging.warn('Adding thread failed for {}, args {}'.format(key, e.args))


# self.read_log(key)

esforwarder = esForwarder()
esforwarder.run()
