##### Motivation
I used E.L.K + logstash-forwarder with 4 logstashes, but as log size getting increased logstash getting heavier and it crashed from time to time. and when I upgrade logstash it deprecated some feature.
To distibute log parsing load to each server, and not to be tackled by logstash I decided to write new forwarder.
The data flow was logfile - logstash-forwarder - logstash - elasticsearch before. But now the flow was simplified up to logfile - es-logforwarder - elasticsearch

#####Installation
copy es_forwarder.py, es_forwarder.ini and pattern.ini in a same directory
configure es_forwarder.ini accordingly
run es_forwarder.py

Basically, it is foreground process, you need to use systemd to run it as daemon-like process

#####Configuration

**es_forwarder.ini** can have only 1 global section and multiple log sections
```ini
[global]
# global section is a mandatory section
# comma separated list of elasticsearch servers
elasticsearch_host = elasticsearchserver1, elasticserver2,........
# port elasticsearch running on
elasticsearch_port = 9200

# section name for each log
[traffic_log]
# pattern for log file format. In case of json, use 'None' here
pattern = (python regular expression | bundle_<pattern_name> | None)
# log file path
file_path = /log/file/path/to/read
# Wildcard can be used for a file name. 
#ie)
#file_path = /var/www/app/logs/default/*.log. In this case es_forwarder lists up all logs which are matched the pattern and uses the last updated log only.
# in case the file has not updated for last 60 seconds, it tries to find newest file again.

# timestamp field name. 
ts_field = timestamp
# index name to use on Elasticsearch. the real index name will be <index_name>-yyyy.mm.dd
index_name = index-name-in-elasticsearch
# timestamp field type. python parser can't parse nginx and apache timestamp. In case reading apache/nginx access_log use *nginx*, or *None* will be enough 
# in case of the timestamp is in unixtimestamp, use *unix*
ts_type = nginx | unix | None

# optional below
# ip address field name( Optional ). if IP address exists and you want to use geological coordinate, give the ip field name
# We are using commercial service, so it will not be supported here
geoip = ip 
# geoip needs parsing server. In case geoip has a field name, it's mandatory) 
# geoIP parsing server = 127.0.0.1

# sampling rate in percent. only *x* percent of logs are sent to elasticsearch Default is 100. It should not be greater than 100
# It doesn't use exact percent, but approximately measured
sampling_rate = 100
# log_format support text|json. text is a default
log_format = text | json 
# type of a document. default is a log.
doc_type = log

# Change timezone. In case of the source timezone has a different timezone rather than UTC. use below
# in case source timezone in Paris
ts_tz = Europe/Paris

```
**patterns.ini** has a bundled patterns. If *pattern* in log section in *es_forwarder.ini* starts with **bundle_<pattern_name>**, then it finds the <pattern_name> from this file

```ini
[log_patterns]
# this one will be named **bundle_olamobileweblog** in the es_forwarder.ini
apachecustomlog = (?P<ip>\S+) - (?P<auth>\S+) \[(?P<timestamp>\d+/\w+/\d+:\d+:\d+:\d+ \+\d+)\] "(?P<verb>\S+) (?P<request>[\W\w]+)" (?P<response>\d+) (?P<size>\d+) "(?P<referrer>|[\W\w]+)" "(?P<browser>[\W\w+]+)" (?P<responsetime>[0-9.]+) (?P<vhost>\S+) (?P<upstream>[0-9.-]+) (?P<cache>\S+)
# this one will be named **bundle_syslog** in the es_forwarder.ini
syslog = (?P<timestamp>\w+\s+\d+\s+\d+:\d+:\d+)\s+(?P<host>\S+)\s+(?P<daemon>\S+)+\s+(?P<message>.*)
```
#####Limitation
It saves file offset to start from last sent

#####Note
Check Python regular expression from 
https://pythex.org/

Python regular expression 
https://docs.python.org/2/library/re.html

#####for future  needs
Considering to support maxmind DB for geoip
http://dev.maxmind.com/geoip/geoip2/geolite2/

